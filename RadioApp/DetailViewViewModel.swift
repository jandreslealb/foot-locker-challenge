//
//  DetailViewViewModel.swift
//  RadioApp
//
//  Created by Andres Leal on 5/3/21.
//  Copyright © 2021 Andres Leal. All rights reserved.
// (set) = es publica para lectura pero privada para setear un valor
//

import Foundation
import RxSwift


class DetailViewViewModel {
    private var managerConnections = ManagerConnections()
    private(set) weak var view:MainDetailView?
    private var router: DetailRouter?
    
    func bind(view:MainDetailView, router: DetailRouter){
        self.view = view
        self.router = router
        self.router?.setSourceView(view)
    }
    
    func getStationData(movieID: String) -> Observable<RadioStation>{
        return managerConnections.getDetailStation(movieID: movieID)
    }
}
