//
//  DetailRouter.swift
//  RadioApp
//
//  Created by Andres Leal on 5/3/21.
//  Copyright © 2021 Andres Leal. All rights reserved.
//

import UIKit

class DetailRouter {
    
    var urlDescriptionImageRouter: String?
    var detailStationTitleRouter: String?
    var djRouter: String?
    var djEmailRouter: String?
    var listenersRouter: String?
    var genreRouter: String?
    var viewController: UIViewController {
           return createViewController()
    }
       
       private var sourceView: UIViewController?
    
    init(urlDescriptionImageRouter: String? = "",
         detailStationTitleRouter: String? = "",
         djRouter: String? = "",
         djEmailRouter: String? = "",
         listenersRouter: String? = "",
         genreRouter: String? = "") {
        self.urlDescriptionImageRouter = urlDescriptionImageRouter
        self.detailStationTitleRouter = detailStationTitleRouter
        self.djRouter = djRouter
        self.djEmailRouter = djEmailRouter
        self.listenersRouter = listenersRouter
        self.genreRouter = genreRouter
    }
       
       func setSourceView(_ sourceView: UIViewController?){
           guard let view = sourceView else {fatalError("Error desconocido")}
           self.sourceView = view
       }
    
    private func createViewController() -> UIViewController {
        let view = MainDetailView(nibName: "MainDetailView", bundle: Bundle.main)
        view.urlDescriptionImageRouter = self.urlDescriptionImageRouter
        view.detailStationTitleRouter = self.detailStationTitleRouter
        view.djRouter = self.djRouter
        view.djEmailRouter = self.djEmailRouter
        view.listenersRouter = self.listenersRouter
        view.genreRouter = self.genreRouter
        return view
    }
}
