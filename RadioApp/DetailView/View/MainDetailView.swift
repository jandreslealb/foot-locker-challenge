//
//  MainDetailView.swift
//  RadioApp
//
//  Created by Andres Leal on 4/30/21.
//  Copyright © 2021 Andres Leal. All rights reserved.
//

import UIKit
import RxSwift

class MainDetailView: UIViewController {
    @IBOutlet private weak var lblTitle: UILabel!
    @IBOutlet private weak var imgMainFilmImage: UIImageView!
    @IBOutlet private weak var lblOverview: UILabel!
    @IBOutlet private weak var lblReleaseDate: UILabel!
    @IBOutlet private weak var lblOriginalTitle: UILabel!
    @IBOutlet private weak var lblVoteAverage: UILabel!
    
    private var router = DetailRouter()
    private var viewModel = DetailViewViewModel()
    private var disposeBag = DisposeBag()
    var urlDescriptionImageRouter: String?
    var detailStationTitleRouter: String?
    var djRouter: String?
    var djEmailRouter: String?
    var listenersRouter: String?
    var genreRouter: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getDataAndShowDetailStation()
        viewModel.bind(view: self, router: router)
    }
    
    private func getDataAndShowDetailStation(){
        guard let imageStation = urlDescriptionImageRouter else {return}
        guard let titleStation = detailStationTitleRouter else {return}
        guard let dj = djRouter else {return}
        guard let emailDj = djEmailRouter else {return}
        guard let listeners = listenersRouter else {return}
        guard let genre = genreRouter else {return}
        self.imgMainFilmImage.imageFromServerUrl(urlString: imageStation, placeHolderImage: UIImage(named: "RadioImage")!)
        self.lblTitle.text = titleStation
        self.lblOverview.text = dj
        self.lblReleaseDate.text = emailDj
        self.lblOriginalTitle.text = listeners
        self.lblVoteAverage.text = genre
        return viewModel.getStationData(movieID: titleStation).subscribe(onNext: { movie in
    
        }, onError: { (error) in
            print("Ha ocurrido un error: \(error)")
        }, onCompleted: {
            
        }).disposed(by: disposeBag)
    }
}
