//
//  HomeRouter.swift
//  RadioApp
//
//  Created by Andres Leal on 4/24/21.
//  Copyright © 2021 Andres Leal. All rights reserved.
//

// Esta clase creara el objeto home , pero tambien servira de router para ir a otras vistas / pantallas


import Foundation
import UIKit


class HomeRouter {
    var viewController: UIViewController {
        return createViewController()
    }
    
    private var sourceView: UIViewController?
    
    private func createViewController() -> UIViewController {
        let view = HomeView(nibName: "HomeView", bundle: Bundle.main)
        return view
    }
    
    func setSourceView(_ sourceView: UIViewController?){
        guard let view = sourceView else {fatalError("Error desconocido")}
        self.sourceView = view
    }
    
    func navigateToDetailView(urlDescriptionImagevm: String,detailStationTitlevm: String, djvm: String, djEmailvm: String, listenersvm: String, genrevm: String){
        
        let detailView = DetailRouter( urlDescriptionImageRouter: urlDescriptionImagevm,
                                       detailStationTitleRouter: detailStationTitlevm,
                                       djRouter: djvm,
                                       djEmailRouter: djEmailvm,
                                       listenersRouter: listenersvm,
                                       genreRouter: genrevm).viewController
        sourceView?.navigationController?.pushViewController(detailView, animated: true)
    }
}
