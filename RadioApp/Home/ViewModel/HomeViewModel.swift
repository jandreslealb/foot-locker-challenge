//
//  HomeViewModel.swift
//  RadioApp
//
//  Created by Andres Leal on 4/24/21.
//  Copyright © 2021 Andres Leal. All rights reserved.
//
// weak para evitar ciclos de retenciones de memoria

import Foundation
import RxSwift

class HomeViewModel{
    private weak var view: HomeView?
    private var router: HomeRouter?
    private var managerConnections = ManagerConnections()
    
    
    func bind (view: HomeView, router: HomeRouter){
        self.view = view
        self.router = router
        self.router?.setSourceView(view)
        // bindear el router con la vista 
    }
    
    func getListMoviesData() -> Observable<[Station]>{
        return managerConnections.getRadioStation()
    }
    
    func makeDetailView(urlDescriptionImage: String ,detailStationTitle: String,dj: String, djEmail: String, listeners: String, genre: String ){
        router?.navigateToDetailView(urlDescriptionImagevm: urlDescriptionImage , detailStationTitlevm: detailStationTitle, djvm: dj, djEmailvm: djEmail, listenersvm: listeners, genrevm: genre)
    }
}
