//
//  HomeView.swift
//  RadioApp
//
//  Created by Andres Leal on 4/24/21.
//  Copyright © 2021 Andres Leal. All rights reserved.
//
// tableView.dequeueReusableCell(withIdentifier: "Cell")!
//  cell.textLabel?.text = movies[indexPath.row].originalTitle
// String(describing: MainTableCustomViewCell.self)
// Datatask .resume al final

import UIKit
import RxSwift
import RxCocoa

class HomeView: UIViewController {
    
    @IBOutlet private weak var mainTableView: UITableView!
    private var router = HomeRouter()
    @IBOutlet private weak var loadActivity: UIActivityIndicatorView!
    private var viewModel = HomeViewModel()
    private var disposeBag = DisposeBag()
    private var movies = [Station]()
    private var filteredMovies = [Station]()
    
    lazy var searchController: UISearchController = ({
        let controller = UISearchController(searchResultsController: nil)
        controller.hidesNavigationBarDuringPresentation = true
        controller.obscuresBackgroundDuringPresentation = false
        controller.searchBar.sizeToFit()
        controller.searchBar.barStyle = .black
        controller.searchBar.backgroundColor = .clear
        controller.searchBar.placeholder = "Search a radio station"
        
        return controller
    })()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Streaming Radio"
        configureTableView()
        viewModel.bind(view: self, router: router)
        getData()
        manageSearchBarController()
    }
    
    private func configureTableView(){
        mainTableView.rowHeight = UITableView.automaticDimension
        mainTableView.register(UINib(nibName: "MainTableCustomViewCell", bundle: nil), forCellReuseIdentifier: "MainTableCustomViewCell")
    }
    
    private func getData(){
        return viewModel.getListMoviesData()
            // Manejar la concurrencia o hilos de RxSwift
            .subscribe(on: MainScheduler.instance)
            .observe(on: MainScheduler.instance)
            // Suscribirme a el observable
            .subscribe(
                onNext: { movies in
                    self.movies = movies
                    self.reloadTableView()
            }, onError: { error in
                print(error.localizedDescription)
            }, onCompleted: {
                print("completado")
            }).disposed(by: disposeBag)
        
        // Dar por completado la secuencia de RXSwift
        //
    }
    
    private func reloadTableView(){
        DispatchQueue.main.async {
            self.loadActivity.stopAnimating()
            self.loadActivity.isHidden = true
            self.mainTableView.reloadData()
        }
    }
    private func manageSearchBarController(){
        let searchBar = searchController.searchBar
        searchController.delegate = self
        mainTableView.tableHeaderView = searchBar
        mainTableView.contentOffset = CGPoint(x: 0, y: searchBar.frame.size.height)
        
        searchBar.rx.text
            .orEmpty
            .distinctUntilChanged()
            .subscribe(onNext: { (result) in
                self.filteredMovies = self.movies.filter({ movie in
                   // self.mainTableView.reloadData()
                    self.reloadTableView()
                    return movie.title.contains(result)
                })
            }).disposed(by: disposeBag)
    }
}

extension HomeView: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return self.filteredMovies.count
        } else {
            return self.movies.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainTableCustomViewCell") as! MainTableCustomViewCell
        
        
        if searchController.isActive && searchController.searchBar.text != "" {
            cell.imgMovie.imageFromServerUrl(urlString: filteredMovies[indexPath.row].image , placeHolderImage: UIImage(named: "RadioImage")!)
            cell.lblTitleMovie.text = filteredMovies[indexPath.row].title
            cell.lblDescriptionMovie.text = filteredMovies[indexPath.row].channelDescription
        } else {
            
            cell.imgMovie.imageFromServerUrl(urlString: movies[indexPath.row].image, placeHolderImage: UIImage(named: "RadioImage")!)
            cell.lblTitleMovie.text = movies[indexPath.row].title
            cell.lblDescriptionMovie.text = movies[indexPath.row].channelDescription
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if searchController.isActive && searchController.searchBar.text != "" {
            viewModel.makeDetailView(urlDescriptionImage:String(self.filteredMovies[indexPath.row].image),
                                     detailStationTitle: String(self.filteredMovies[indexPath.row].title),
                                     dj: String(self.filteredMovies[indexPath.row].dj),
                                     djEmail: String(self.filteredMovies[indexPath.row].djmail),
                                     listeners: String(self.filteredMovies[indexPath.row].listeners),
                                     genre: String(self.filteredMovies[indexPath.row].genre))
        } else {
            viewModel.makeDetailView(urlDescriptionImage: String(self.movies[indexPath.row].image) ,detailStationTitle: String(self.movies[indexPath.row].title),
                                     dj: String(self.movies[indexPath.row].dj),
                                     djEmail: String(self.movies[indexPath.row].djmail),
                                     listeners: String(self.movies[indexPath.row].listeners),
                                     genre: String(self.movies[indexPath.row].genre))
        }
    }
}

extension HomeView: UISearchControllerDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        searchController.isActive = false
        reloadTableView()
    }
}
