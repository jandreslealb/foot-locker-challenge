//
//  RadioStation.swift
//  RadioApp
//
//  Created by Andres Leal on 4/28/21.
//  Copyright © 2021 Andres Leal. All rights reserved.
//

import Foundation

// MARK: - Welcome
struct RadioStation: Codable {
    let listOfStation: [Station]
    
    enum CodingKeys: String, CodingKey {
        case listOfStation = "channels"
    }
}

// MARK: - Channel
struct Station: Codable {
    let id, title, channelDescription, dj: String
    let djmail, genre: String
    let image: String
    let largeimage: String?
    let xlimage: String
    let twitter, updated: String
    let playlists: [Playlist]
    let listeners, lastPlaying: String

    enum CodingKeys: String, CodingKey {
        case id, title
        case channelDescription = "description"
        case dj, djmail, genre, image, largeimage, xlimage, twitter, updated, playlists, listeners, lastPlaying
    }
}

// MARK: - Playlist
struct Playlist: Codable {
    let url: String
    let format: Format
    let quality: Quality
}

enum Format: String, Codable {
    case aac = "aac"
    case aacp = "aacp"
    case mp3 = "mp3"
}

enum Quality: String, Codable {
    case high = "high"
    case highest = "highest"
    case low = "low"
}
