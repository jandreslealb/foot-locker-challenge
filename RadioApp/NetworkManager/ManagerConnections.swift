//
//  ManagerConnections.swift
//  RadioApp
//
//  Created by Andres Leal on 4/28/21.
//  Copyright © 2021 Andres Leal. All rights reserved.
//

import Foundation
import RxSwift

class ManagerConnections{
    func getRadioStation() -> Observable<[Station]> {
        
        return Observable.create { observer in
            
            let session = URLSession.shared
            var request = URLRequest(url: URL(string: Constants.URL.main+Constants.EndPoints.urlListRadioStation)!)
            request.httpMethod = "GET"
            request.addValue("application/json", forHTTPHeaderField: "content-Type")
        
            session.dataTask(with: request) { (data, response, error) in

                guard let data = data, error == nil, let reponse = response as? HTTPURLResponse else { return }
                
                if reponse.statusCode == 200 {
                    do {
                        let decoder = JSONDecoder()
                        let station = try decoder.decode(RadioStation.self, from: data)
                    
                        observer.onNext(station.listOfStation)
                    } catch let error {
                        observer.onError(error)
                        print("ha ocurrido un error \(error.localizedDescription)" )
                    }
                    
                } else if reponse.statusCode == 401 {
                    print("Error 401")
                }
                observer.onCompleted()
            }.resume()
            
            return Disposables.create {
                session.finishTasksAndInvalidate()
            }
        }
    }
    
    func getDetailStation(movieID: String) -> Observable <RadioStation>{
        
        return Observable.create { observer in
            
            let session = URLSession.shared
            var request = URLRequest(url: URL(string: Constants.URL.main+Constants.EndPoints.urlListRadioStation)!)
    
            request.httpMethod = "GET"
            request.addValue("application/json", forHTTPHeaderField: "content-Type")
            
            session.dataTask(with: request) { (data, response, error) in
                
                guard let data = data, error == nil, let reponse = response as? HTTPURLResponse else { return }
                
                if reponse.statusCode == 200 {
                    do {
                        let decoder = JSONDecoder()
                        let detailMovie = try decoder.decode(RadioStation.self, from: data)
                
                        observer.onNext(detailMovie)
                    } catch let error {
                        observer.onError(error)
                        print("ha ocurrido un error \(error.localizedDescription)" )
                    }
                    
                } else if reponse.statusCode == 401 {
                    print("Error 401")
                }
                observer.onCompleted()
            }.resume()
            
            return Disposables.create {
                session.finishTasksAndInvalidate()
            }
        }
        
    }
}
